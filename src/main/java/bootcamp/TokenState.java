package bootcamp;


import com.google.common.collect.ImmutableList;
import lombok.*;
import net.corda.core.contracts.*;
import net.corda.core.identity.*;
import net.corda.core.serialization.CordaSerializable;

import javax.validation.constraints.NotNull;
import java.util.List;

/* Our state, defining a shared fact on the ledger.
 * See src/main/java/examples/ArtState.java for an example. */
@Getter
@Setter
@CordaSerializable
@AllArgsConstructor
@BelongsToContract(TokenContract.class)
public class TokenState implements ContractState {
    private Party issuer;
    private Party owner;
    private int amount;

    @Override
    @NotNull
    public List<AbstractParty> getParticipants() {
        return ImmutableList.of(issuer, owner);
    }
}