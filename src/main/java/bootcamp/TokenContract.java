package bootcamp;

import net.corda.core.contracts.*;
import net.corda.core.transactions.LedgerTransaction;
import org.jetbrains.annotations.NotNull;

/* Our contract, governing how our state will evolve over time.
 * See src/main/java/examples/ArtContract.java for an example. */
public class TokenContract implements Contract {
    public static String ID = TokenContract.class.getName();


    @Override
    public void verify(@NotNull LedgerTransaction tx) throws IllegalArgumentException {
        if (tx.getInputStates().size() != 0) throw new IllegalArgumentException("Zero Inputs Expected.");
        if (tx.getOutputs().size() != 1) throw new IllegalArgumentException("One Output Expected.");
        if (tx.getCommands().size() != 1) throw new IllegalArgumentException("One Command Expected.");
        if (!(tx.getOutput(0) instanceof TokenState))
            throw new IllegalArgumentException("Output of type TokenState Expected.");
        if (!(tx.getCommand(0).getValue() instanceof Commands.Issue))
            throw new IllegalArgumentException("Issue Command Expected.");
        TokenState outputState = (TokenState) tx.getOutput(0);
        if (outputState.getAmount() < 1) throw new IllegalArgumentException("Amount must be positive.");
        if (!(tx.getCommand(0).getSigners().contains(outputState.getIssuer().getOwningKey())))
            throw new IllegalArgumentException("Issuer must be a signer.");

    }


    public interface Commands extends CommandData {
        class Issue implements Commands {}
//        class Transfer implements Commands {}
    }
}